#include "scene.h"
#include "callbacks.h"

#include <GL/glut.h>
#include <SOIL/SOIL.h>

#include <load.h>

int fogsets = 0;

double fogdens = 0.0f;

void init_scene(Scene* scene)
{
    load_model(&(scene->plane), "models/plane.obj");
    scene->texture_id = load_texture("textures/plane.jpg");
	
	scene->texture_help = load_texture("textures/help.png");
	
	scene->texture_space = load_texture("textures/sky.jpg");
	
	
    scene->material.ambient.red = 0.2;
    scene->material.ambient.green = 0.2;
    scene->material.ambient.blue = 0.2;

    scene->material.diffuse.red = 0.6;
    scene->material.diffuse.green = 0.6;
    scene->material.diffuse.blue = 0.6;

    scene->material.specular.red = 1.0;
    scene->material.specular.green = 1.0;
    scene->material.specular.blue = 1.0;

    scene->material.shininess = 10.0;
	
	set_lighting();
}

void set_lighting()
{
	float ambient_light[] = { 0.8f, 0.8f, 0.8f, 0.8f };
	float diffuse_light[] = { 0.9f, 0.9f, 0.9f, 0.9f };
	float specular_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float position[] = { 0.0f, 0.0f, 10.0f, 1.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
	
}

void set_material(const Material* material)
{
    float ambient_material_color[] = {
        material->ambient.red,
        material->ambient.green,
        material->ambient.blue
    };

    float diffuse_material_color[] = {
        material->diffuse.red,
        material->diffuse.green,
        material->diffuse.blue
    };

    float specular_material_color[] = {
        material->specular.red,
        material->specular.green,
        material->specular.blue
    };

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_material_color);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &(material->shininess));
}

void decrease_fog()
{
	if ( fogdens > 0.0f ){	
	int i;
	for ( i = 0; i < 8; ++i){
		fogdens -= 0.0001f;
		}
	glEnable(GL_FOG);
}	
}

void increase_fog()
{
if ( fogdens < 1.0f ){	
	int i;
	for ( i = 0; i < 4; ++i){
		fogdens += 0.0001f;
		}
	glEnable(GL_FOG);
}	
}

void fog()
{
	float fogColor[] = {0.5, 0.5, 0.5, 1.0};
		
		glEnable(GL_FOG);
		glHint(GL_FOG_HINT, GL_NICEST);
		glFogi(GL_FOG_MODE, GL_EXP2);
		glFogf(GL_FOG_DENSITY, fogdens);
		glFogf(GL_FOG_START, 1);
		glFogf(GL_FOG_END, 1000.0);
		glFogfv(GL_FOG_COLOR, fogColor);
}


void draw_sky()
{
	
	GLUquadric* pSphereQuadric = gluNewQuadric();
	gluQuadricDrawStyle( pSphereQuadric, GLU_FILL );
	gluQuadricOrientation( pSphereQuadric, GLU_INSIDE );
	gluQuadricTexture( pSphereQuadric, GL_TRUE );
	gluQuadricNormals( pSphereQuadric, GLU_SMOOTH );
	gluSphere( pSphereQuadric, 300.0, 1000, 600 );
	gluDeleteQuadric( pSphereQuadric );	
}

void draw_scene(const Scene* scene)
{
    set_material(&(scene->material));
	
	double move = calc_test();
	glBindTexture( GL_TEXTURE_2D, scene->texture_id);
	glPushMatrix();
	glRotatef(270, 0, 1, 0);
	glTranslatef(0, -move, 0);
	draw_model(&(scene->plane));
	glPopMatrix();
	fog();

	glBindTexture( GL_TEXTURE_2D, scene->texture_space);
	glPushMatrix();
	glRotatef(180, 0, 1, 0);
	draw_sky();
	glPopMatrix();
	
	glBindTexture( GL_TEXTURE_2D, scene->texture_help);
}

double calc_elapsed_time() {
	
	static int last_frame_time = 0;
	int current_time;
	double elapsed_time;

	current_time = glutGet(GLUT_ELAPSED_TIME);
	elapsed_time = (double)(current_time - last_frame_time) / 1000;
	last_frame_time = current_time;
	return elapsed_time;
}