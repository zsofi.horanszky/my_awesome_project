#include "callbacks.h"
#include "init.h"
#include "texture.h"
#include "scene.h"

#include <GL/glut.h>

#include <stdio.h>

void set_callbacks()
{
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboard_up);
	glutSpecialFunc(special_key_handler);
    glutIdleFunc(idle);
}

int main(int argc, char* argv[])
{
    int window;
    glutInit(&argc, argv);

    glutInitWindowSize(640, 480);     
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	
    window = glutCreateWindow("REPULOGEP, F1 SUGO");
    glutSetWindow(window);
	
    init_opengl();
    init_scene(&scene);
    init_camera(&camera);
    set_callbacks();
	
	glutMainLoop();

    return 0;
}